# Introduction
* In this module, we are going to build an application that save, extract, and calculate cars years average.
* an XLS dataset from [here](http://courses.washington.edu/hcde511/s14/datasets/cars.xls) is used as preloaded data.

# Prerequisite 
* [Docker](https://docs.docker.com/get-docker/)
* for **windows**  you need to install [Make](http://gnuwin32.sourceforge.net/packages/make.htm)

# Configuration
* you can configure go client from **client/config**
* `config.dev.yml` for local configuration 
* `config.prod.yml` for the cluster configuration. 

# Run the project

### this command will run docker-compose , create `cars` table in clickhouse database, and insert the csv cars dataset it the `cars` table.

```sh
    make build
``` 
# GUI
### [tabix](https://github.com/tabixio/tabix) is used as a gui client for clickhouse
1. open `http://127.0.0.1:8080` 
2. name : `dev` 
3. host : `http://127.0.0.1:8123`
4. Login : `default`
5. Password : empty
6. click `sign in`

# Go client 
### go client creates three endpoints that executes queries in clickhouse database.
1. open Postman and create a **GET** request to `http://127.0.0.1:8888` to get all preloaded cars from the database in json format.
2. create a **POST** request to `http://127.0.0.1:8888/car` with a car json model. 
```json
  {
    "Model": "bmw",
    "MPG": 18,
    "Cylinders": 15,
    "Displacement": 300,
    "HorsePower": 70,
    "Weight": 3000,
    "Acceleration": 180,
    "Year": 10,
    "Origin": "DE"
  }
```
:warning: to test error handling, `Model` and `Origin` are required , and year must be <= 200.

3. create a **GET** request to `http://127.0.0.1:8888/average` to get the average of cars years

4. stop the project
```sh
    make stop
```

# Test and Run Go client locally

* you have to stop the project by running `make stop` before you proceed.

* Make sure you have `Go` installed and configured in your path.

##### 1. create network 
```sh
 make create-network
 ```
##### 2. start clickhouse server instance 
```sh
 make start-clickhouse
 ```
 ##### 3. create cars database
```sh
 make create-table
 ```
 ##### 4. load dataset from csv
```sh
 make load-data-set
 ```
##### 5. navigate to client directory 
```sh
cd client
```
##### 6. Download project dependencies
```sh
make init
```
##### 7. Run tests
```sh
make test
```
##### 8. Build the server
this will create a bin folder contain the executable client

```sh
make build-server
```
##### 9. Run the server
this will run the server with dev configuration on `http://127.0.0.1:8888`
```sh
make run-server
```
or you can run the builded server
```sh
make run-build
```