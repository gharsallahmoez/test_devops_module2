package config_test

import (
	"os"
	"testing"

	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	tt "gitlab.com/gharsallahmoez/test_devops_module2/src/testData/config"
)

const ConfEnvVar = "CONFIGOR_ENV"

// Test getting configuration from config.yal file and parse it to config struct
func TestMakeConfig(t *testing.T) {
	for _, testCase := range tt.CreateTTMakeConf() {
		t.Run(testCase.Name, func(t *testing.T) {
			os.Setenv(ConfEnvVar, testCase.EnvVar)
			conf, err := config.MakeConfig()
			if err != nil ||
				(testCase.IsProd && conf.Parameters.Env != "prod") ||
				(testCase.IsDev && conf.Parameters.Env != "dev") {
				t.Errorf("expected loading config for %v environment, got error: %v", testCase.EnvVar, err)
			}
		})
	}
}
