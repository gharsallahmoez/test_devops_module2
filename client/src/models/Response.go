package models

// GenericResponse model
type GenericResponse struct {
	Error     string
	Timestamp uint64
}
