package models

// Car model
type CarModel struct {
	Model        string  `db:"model"`
	MPG          float32 `db:"mpg"`
	Cylinders    uint32  `db:"cylinders"`
	Displacement float32 `db:"displacement"`
	HorsePower   float32 `db:"horsePower"`
	Weight       float32 `db:"weight"`
	Acceleration float32 `db:"acceleration"`
	Year         uint32  `db:"year"`
	Origin       string  `db:"origin"`
}
