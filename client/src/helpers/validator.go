package helpers

import (
	"gitlab.com/gharsallahmoez/test_devops_module2/src/models"
	"strings"
)

// IsCarModelEmpty validates Car model
func IsCarModelEmpty(car *models.CarModel) bool {
	return isStringEmpty(car.Model) || isStringEmpty(car.Origin) || car.Year > 200
}

// isStringEmpty checks if a string is empty.
func isStringEmpty(input string) bool {
	return len(strings.TrimSpace(input)) == 0
}
