package database

import (
	"errors"
	"fmt"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/database/clickhouse"
	mdl "gitlab.com/gharsallahmoez/test_devops_module2/src/models"
)

// CarHandler interface for db layer
type CarHandler interface {
	// AddCar adds new car to the database
	AddCar(car mdl.CarModel) error
	// GetAllCars get all cars from database
	GetAllCars() ([]mdl.CarModel, error)
}

// DbHandler holds the handler for car model
type DbHandler interface {
	CarHandler
}

// Create creates db handler based on the given config
func Create(carConfig *config.Car) (DbHandler, error) {
	var carHandler CarHandler
	var err error
	switch carConfig.Type {
	case "clickhouse":
		carHandler = clickhouse.NewRepo(carConfig)
	default:
		return nil, errors.New(fmt.Sprintf("%s is an unknown database type", carConfig.Type))
	}
	handlers := struct {
		CarHandler
	}{
		carHandler,
	}
	return handlers, err
}
