package database_test

import (
	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	handler "gitlab.com/gharsallahmoez/test_devops_module2/src/database"
	td "gitlab.com/gharsallahmoez/test_devops_module2/src/testData/database"
	"testing"
)

// TestCreate tests Create function in the handler
func TestCreate(t *testing.T) {
	tableTest := td.CreateTTHandler()
	for _, testCase := range tableTest {
		conf := config.Database{
			Car: testCase.CarDb,
		}
		t.Run(testCase.Name, func(t *testing.T) {
			_, err := handler.Create(&conf.Car)
			if err != nil && !testCase.HasError {
				t.Errorf("expected success , got error: %v", err)
			}
			if err == nil && testCase.HasError {
				t.Error("expected error, got nil")
			}
		})
	}
}
