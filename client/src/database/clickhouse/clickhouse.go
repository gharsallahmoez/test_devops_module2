package clickhouse

import (
	"fmt"
	"github.com/ClickHouse/clickhouse-go"
	"github.com/jmoiron/sqlx"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/helpers"
	mdl "gitlab.com/gharsallahmoez/test_devops_module2/src/models"
)

type Repo struct {
	conf   *config.Car
	client *sqlx.DB
}

// NewRepo creates a clickhouse client
func NewRepo(dbConf *config.Car) *Repo {
	client, err := connect(dbConf)
	if err != nil {
		fmt.Errorf("unable to connect %v", err)
	}
	repo := Repo{
		conf:   dbConf,
		client: client,
	}
	return &repo
}

// connect open connection with clickhouse server and create Car table
func connect(dbConf *config.Car) (*sqlx.DB, error) {
	client, err := sqlx.Open(dbConf.Type, dbConf.Host)
	if err != nil {
		return nil, err
	}
	if err := client.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			fmt.Println(err)
		}
		return nil, err
	}
	return client, err
}

func (r Repo) AddCar(car mdl.CarModel) error {
	if helpers.IsCarModelEmpty(&car) {
		return fmt.Errorf("please enter a valid input")
	}
	query := fmt.Sprintf("INSERT INTO %s (model, mpg, cylinders,displacement, horsePower, weight, acceleration, year, origin) VALUES (?, ?, ?, ?, ?, ?, ?, ? , ?)", r.conf.TableName)
	tx, err := r.client.Begin()
	if err != nil {
		return fmt.Errorf("unable to insert data %s", err)
	}
	stmt, err := tx.Prepare(query)
	if err != nil {
		return fmt.Errorf("unable to insert data %s", err)
	}

	defer stmt.Close()

	if _, err := stmt.Exec(
		car.Model,
		car.MPG,
		car.Cylinders,
		car.Displacement,
		car.HorsePower,
		car.Weight,
		car.Acceleration,
		car.Year,
		car.Origin,
	); err != nil {
		return fmt.Errorf("unable to insert data %s", err)
	}

	err = tx.Commit()

	return nil

}

func (r Repo) GetAllCars() ([]mdl.CarModel, error) {
	var items []mdl.CarModel
	query := fmt.Sprintf("SELECT model,mpg,cylinders,displacement,horsePower,weight,acceleration,year,origin  FROM %s", r.conf.TableName)

	if err := r.client.Select(&items, query); err != nil {
		return nil, err
	}

	return items, nil
}
