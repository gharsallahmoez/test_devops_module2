package clickhouse

import (
	"log"
	"os"
	"testing"

	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	td "gitlab.com/gharsallahmoez/test_devops_module2/src/testData/database"
)

var (
	repo *Repo
)

func TestMain(m *testing.M) {
	conf, err := config.MakeConfig()
	if err != nil {
		log.Fatalf("unable to load config %v", err)
	}
	repo = NewRepo(&conf.Database.Car)
	code := m.Run()
	os.Exit(code)
}

// TestConnect tests connect function in db layer
func TestConnect(t *testing.T) {
	t.Run("test connect", func(t *testing.T) {
		conf, _ := config.MakeConfig()
		carDB := conf.Database.Car
		_, err := connect(&carDB)
		if err != nil {
			t.Errorf("expected connect got error: %v", err)
		}
	})
}

// TestRepo_GetAllCars tests GetAllCars function in db layer
func TestRepo_GetAllCars(t *testing.T) {

	t.Run("expect get all cars", func(t *testing.T) {
		// insert car for test
		err := repo.AddCar(td.AddCarData()[0].Car)
		if err != nil {
			t.Errorf("cannot insert car for test  %v", err)
		}
		resp, err := repo.GetAllCars()
		if err != nil {
			t.Errorf("expected get all cars, got error %v", err)
		}
		if len(resp) == 0 {
			t.Error("expected get all cars, got 0")
		}
	})

}

// TestRepo_AddCar tests add car function in db layer
func TestRepo_AddCar(t *testing.T) {
	tt := td.AddCarData()
	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {

			err := repo.AddCar(tc.Car)

			if err != nil && !tc.HasError {
				t.Errorf("expected success got error: %v", err)
			}
			if err == nil && tc.HasError {
				t.Error("expected failure got nil")
			}

		})
	}

}
