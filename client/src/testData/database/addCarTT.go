package database

import (
	"gitlab.com/gharsallahmoez/test_devops_module2/src/models"
)

// TTCar represents table test structure of AddCar test.
type TTCar struct {
	Name     string
	Car      models.CarModel
	HasError bool
}

// AddCarData creates table test for AddCar test.
func AddCarData() []TTCar {
	return []TTCar{
		{
			Name: "valid request",
			Car: models.CarModel{
				Model:        "Kia",
				MPG:          14,
				Cylinders:    20,
				Displacement: 1,
				HorsePower:   15,
				Weight:       200,
				Acceleration: 180,
				Year:         70,
				Origin:       "Test",
			},
			HasError: false,
		},
		{
			Name: "invalid request : missing car model",
			Car: models.CarModel{
				Model:        "",
				MPG:          14,
				Cylinders:    20,
				Displacement: 1,
				HorsePower:   15,
				Weight:       200,
				Acceleration: 180,
				Year:         70,
				Origin:       "Test",
			},
			HasError: true,
		},
		{
			Name: "invalid request : invalid year",
			Car: models.CarModel{
				Model:        "Kia",
				MPG:          14,
				Cylinders:    20,
				Displacement: 1,
				HorsePower:   15,
				Weight:       200,
				Acceleration: 180,
				Year:         2000,
				Origin:       "Test",
			},
			HasError: true,
		},
		{
			Name: "invalid request : missing origin",
			Car: models.CarModel{
				Model:        "Kia",
				MPG:          14,
				Cylinders:    20,
				Displacement: 1,
				HorsePower:   15,
				Weight:       200,
				Acceleration: 180,
				Year:         70,
				Origin:       "",
			},
			HasError: true,
		},
	}
}
