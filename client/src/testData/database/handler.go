package database

import "gitlab.com/gharsallahmoez/test_devops_module2/src/config"

// TTCreateHandler represents table test structure of CreateHandler test
type TTCreateHandler struct {
	Name       string
	CarDb config.Car
	HasError   bool
}

// CreateTTHandler creates table test for Create test
func CreateTTHandler() []TTCreateHandler {
	conf, _ := config.MakeConfig()
	return []TTCreateHandler{
		{
			Name: "valid config for car with clickhouse",
			CarDb: config.Car{
				Type:     "clickhouse",
				Host:     conf.Database.Car.Host,
				TableName:conf.Database.Car.TableName,
				Username: conf.Database.Car.Username,
				Password: conf.Database.Car.Password,
			},
			HasError: false,
		},

		{
			Name: "unsupported database for Car",
			CarDb: config.Car{
				Type:     "unknown",
				Host:     conf.Database.Car.Host,
				TableName:conf.Database.Car.TableName,
				Username: conf.Database.Car.Username,
				Password: conf.Database.Car.Password,
			},
			HasError: true,
		},

	}
}