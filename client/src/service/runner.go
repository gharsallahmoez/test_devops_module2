package service

import (
	"errors"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/service/http"
)

type Runner interface {
	Start()
}

// Create creates a runner of type defined in config
func Create(conf *config.Config) (Runner, error) {
	var srv Runner

	switch conf.Server.Type {
	case "http":
		srv = http.NewHttpRunner(*conf)
	default:
		return nil, errors.New("invalid Server Type)")
	}
	return srv, nil
}
