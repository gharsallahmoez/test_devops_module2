package http

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/database"
	"log"
	"net/http"
)

type Runner struct {
	config.Config
	DB database.DbHandler
}

// NewHttpRunner initialize Runner struct
func NewHttpRunner(conf config.Config) *Runner {
	dbHandler, err := database.Create(&conf.Database.Car)
	if err != nil {
		log.Fatalf("Failed to create database handler :%v", err)
	}
	return &Runner{conf, dbHandler}
}

// start starts http server
func (runner Runner) Start() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/car", runner.addNewCar).Methods("POST")
	router.HandleFunc("/average", runner.returnAverageCarsYear)
	router.HandleFunc("/", runner.returnAllCars)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", runner.Config.Server.Port), router))
}
