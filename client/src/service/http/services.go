package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	mdl "gitlab.com/gharsallahmoez/test_devops_module2/src/models"
)

// returnAllCars returns all available cars
func (runner Runner) returnAllCars(w http.ResponseWriter, r *http.Request) {
	cars, _ := runner.DB.GetAllCars()
	json.NewEncoder(w).Encode(cars)
}

// addNewCar save new car
func (runner Runner) addNewCar(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var car mdl.CarModel

	json.Unmarshal(reqBody, &car)

	err := runner.DB.AddCar(car)
	if err != nil {
		resp := mdl.GenericResponse{
			Error:     err.Error(),
			Timestamp: uint64(time.Now().Unix()),
		}
		json.NewEncoder(w).Encode(resp)
		return
	}
	json.NewEncoder(w).Encode(car)
}

// GetAverageCarsYear returns the average of cars year
func (runner Runner) returnAverageCarsYear(w http.ResponseWriter, r *http.Request) {
	cars, _ := runner.DB.GetAllCars()
	var sum uint32
	for _, item := range cars {
		sum += item.Year
	}
	avg := (float32(sum)) / (float32(len(cars)))
	json.NewEncoder(w).Encode(avg)
}
