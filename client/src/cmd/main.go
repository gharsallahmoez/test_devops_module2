package main

import (
	"log"

	"gitlab.com/gharsallahmoez/test_devops_module2/src/config"
	"gitlab.com/gharsallahmoez/test_devops_module2/src/service"
)

// main load config and run the server
func main() {
	conf, err := config.MakeConfig()
	if err != nil {
		log.Fatalf("unable to load server config %v", err)
	}
	runner, err := service.Create(conf)
	if err != nil {
		log.Fatalf("unable to create the server %v", err)
	}
	runner.Start()
}
