module gitlab.com/gharsallahmoez/test_devops_module2

go 1.14

require (
	github.com/ClickHouse/clickhouse-go v1.4.3
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/configor v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	google.golang.org/appengine v1.6.7 // indirect
)
