VERSION=1.0.0
NETWORK=test_devops_module2_default
DATA_SET=data/cars.csv
TABLE=cars

build: compose-up create-table load-data-set

stop: compose-down 

compose-up:
	docker-compose up -d --build

compose-down:
	docker-compose down

build-storage:
	cd storage && docker-compose up --build 

stop-storage:
	cd storage && docker-compose down

create-network:
	docker network create ${NETWORK}


remove-network:
	docker network remove ${NETWORK}

create-table:
	docker run -i --rm --link clickhouse:clickhouse-client --net ${NETWORK} yandex/clickhouse-client -m --host clickhouse --query="CREATE TABLE ${TABLE} \
( \
  model String, \
  mpg Float32, \
  cylinders UInt32, \
  displacement Float32, \
  horsePower Float32, \
  weight Float32, \
  acceleration Float32, \
  year UInt32, \
  origin String \
) ENGINE = Log" 

load-data-set:
	cat ${DATA_SET} | docker run -i --rm --link clickhouse:clickhouse-client --net ${NETWORK} yandex/clickhouse-client -m --host clickhouse \
   --query="INSERT INTO default.${TABLE} FORMAT CSV"

start-clickhouse:
	docker run -d --name clickhouse -p 8123:8123 -p 9000:9000 -p 9009:9009 --net ${NETWORK} --ulimit nofile=262144:262144 yandex/clickhouse-server

stop-clickhouse:
	docker stop clickhouse && docker rm clickhouse
